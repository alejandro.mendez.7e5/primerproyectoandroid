package com.example.affirmations

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ExpandLess
import androidx.compose.material.icons.filled.ExpandMore
import androidx.compose.material.icons.rounded.Call
import androidx.compose.material.icons.rounded.Mail
import androidx.compose.material.icons.rounded.Phone
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import com.example.affirmations.ui.theme.AffirmationsTheme
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.affirmations.model.Affirmation
import com.example.affirmations.ui.theme.DetailViewModel

class DetailActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val intValue : Int = intent.getIntExtra("id", 0)

        setContent {
            AffirmationDetail(intValue)
        }
    }
}

@Composable
fun AffirmationDetail(intValue: Int, detailViewModel : DetailViewModel = viewModel()) {

    detailViewModel.getCards(intValue)
    val uiState by detailViewModel.uiState.collectAsState()
    val context = LocalContext.current

    AffirmationsTheme() {
        uiState?.let { affirmation ->
            Column(
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally,
                modifier = Modifier.fillMaxWidth()
            ) {
                Image(
                    painter = painterResource(id = affirmation.imageResourceId),
                    contentDescription = "image",
                    modifier = Modifier
                        .padding(16.dp)
                        .size(200.dp)
                        .clip(RoundedCornerShape(50)),
                    contentScale = ContentScale.Crop,

                    )
                Text(
                    text = stringResource(id = affirmation.stringResourceId),
                    textAlign = TextAlign.Center,
                    fontWeight = FontWeight.ExtraBold,
                    style = MaterialTheme.typography.h5,
                    modifier = Modifier.padding(16.dp)
                )
                Text(
                    text = stringResource(id = affirmation.stringResourceText),
                    style = MaterialTheme.typography.body2,
                    modifier = Modifier.padding(16.dp)
                )
                Row() {
                    /*
                }
                    TextButton(onClick = {
                        //context.sendMail(affirmation.to, affirmation.subject)
                    }) {
                        Text("Mail")
                    }*/
                    IconButton(onClick = {context.sendMail(affirmation.to, affirmation.subject)}
                    ) {
                        Icon(
                            imageVector = Icons.Rounded.Mail,
                            tint = MaterialTheme.colors.secondary,
                            contentDescription = "Boton desplegable",
                            modifier = Modifier.requiredWidth(50.dp)
                        )
                    }
                    Spacer(Modifier.padding(38.dp))
                    IconButton(onClick = {context.callPhone(affirmation.phone)}
                    ) {
                        Icon(
                            imageVector = Icons.Rounded.Call,
                            tint = MaterialTheme.colors.secondary,
                            contentDescription = "Boton desplegable",
                            modifier = Modifier.requiredWidth(50.dp)
                        )
                    }
                }
        }
        }
    }
}

fun Context.sendMail(to: String, subject: String){
    try {
        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "message/rfc822"
        intent.putExtra(Intent.EXTRA_EMAIL, arrayOf(to))
        intent.putExtra(Intent.EXTRA_SUBJECT, subject)
        startActivity(intent)
    } catch (t: Throwable){
        println("No funcionó") //<- Para que no se quede vacío y de warning
    }
}

fun Context.callPhone(phoneNum: String){
    try {
        val intent = Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phoneNum, null))
        startActivity(intent)
    } catch (t: Throwable){
        println("No funcionó") //<- Para que no se quede vacío y de warning
    }
}


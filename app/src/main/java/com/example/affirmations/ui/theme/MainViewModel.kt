package com.example.affirmations.ui.theme

import androidx.lifecycle.ViewModel
import com.example.affirmations.data.Datasource
import com.example.affirmations.model.Affirmation
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow

class MainViewModel : ViewModel(){
    private val _uiState = MutableStateFlow<List<Affirmation>?>(null)
    val uiState: StateFlow<List<Affirmation>?> = _uiState.asStateFlow()

    fun LoadCards(){
        _uiState.value = Datasource().loadAffirmations()
    }
}
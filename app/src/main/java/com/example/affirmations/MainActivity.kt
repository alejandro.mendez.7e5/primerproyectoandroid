/*
 * Copyright (C) 2021 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.affirmations

import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.animateContentSize
import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.spring
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ExpandLess
import androidx.compose.material.icons.filled.ExpandMore
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import android.content.Intent
import androidx.compose.runtime.*
import androidx.compose.ui.platform.LocalContext
import com.example.affirmations.data.Datasource
import com.example.affirmations.model.Affirmation
import com.example.affirmations.ui.theme.AffirmationsTheme
import androidx.compose.ui.text.style.TextOverflow
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.affirmations.ui.theme.DetailViewModel
import com.example.affirmations.ui.theme.MainViewModel


private const val TAG = "MainActivity"

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            AffirmationApp()
            // TODO 5. Show screen
        }
        Log.d(TAG, "onCreate Called")
    }

    override fun onResume() {
        super.onResume()
        Log.d(TAG, "onResume Called")
    }

    override fun onStart() {
        super.onStart()
        Log.d(TAG, "onStart Called")
    }

    override fun onPause() {
        super.onPause()
        Log.d(TAG, "onPause Called")
    }

    override fun onStop() {
        super.onStop()
        Log.d(TAG, "onStop Called")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(TAG, "onDestroy Called")
    }
}

@Composable
fun AffirmationApp(gameViewModel: MainViewModel = viewModel()) {
    gameViewModel.LoadCards()
    val uiState by gameViewModel.uiState.collectAsState()

    AffirmationsTheme() {
        Column() {/*
                val count = rememberSaveable {
                    mutableStateOf(0)
                }
                TextButton(onClick = {count.value++ }) {
                    Text("Me han pulsado ${count.value}")
                }*/
            if (uiState != null) {
                AffirmationList(affirmationList = uiState!!)
            }
        }
    }
    // TODO 4. Apply Theme and affirmation list
}

@Composable
fun AffirmationList(affirmationList: List<Affirmation>, modifier: Modifier = Modifier) {
    LazyColumn() {
        this.items(items = affirmationList, itemContent = { item ->
            AffirmationCard(affirmation = item)
        })
    }
    // TODO 3. Wrap affirmation card in a lazy column
}

@Composable
fun AffirmationCard(affirmation: Affirmation, modifier: Modifier = Modifier) {
    val expanded = remember { mutableStateOf(false) }
    Card(
        border = BorderStroke(4.dp, MaterialTheme.colors.primary)
    ) {
        Column(
            modifier = Modifier
                .animateContentSize(
                    animationSpec = spring(
                        dampingRatio = Spring.DampingRatioMediumBouncy,
                        stiffness = Spring.StiffnessLow
                    )

                )
        ) {
            Row(
                verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier
                    .fillMaxSize()
                    .padding(16.dp)

            ) {
                Image(
                    painter = painterResource(id = affirmation.imageResourceId),
                    contentDescription = "Imagen de muestra",
                    modifier = Modifier
                        .size(64.dp)
                        .padding(8.dp)
                        .clip(RoundedCornerShape(50.dp)),
                    contentScale = ContentScale.Crop
                )
                Spacer(Modifier.padding(8.dp))
                Text(
                    text = stringResource(affirmation.stringResourceId),
                    fontSize = 13.sp,
                    modifier = Modifier.weight(1f)
                )
                Spacer(Modifier.padding(8.dp))
                DogItemButton(
                    expanded = expanded.value,
                    onClick = {
                        expanded.value = !expanded.value
                    }
                )
            }
            if (expanded.value) {
                Description(affirmation)
            }
        }

    }
    // TODO 1. Your card UI
}

@Preview
@Composable
private fun AffirmationCardPreview() {
    val card: Datasource = Datasource()
    AffirmationCard(affirmation = card.loadAffirmations()[0])
    // TODO 2. Preview your card
}
//var expanded by remember { mutableStateOf(false) }

@Composable
private fun DogItemButton(
    expanded: Boolean,
    onClick: () -> Unit,
    modifier: Modifier = Modifier
) {
    IconButton(onClick = onClick) {
        Icon(
            imageVector =
            if (expanded)
                Icons.Filled.ExpandLess
            else
                Icons.Filled.ExpandMore,
            tint = MaterialTheme.colors.secondary,
            contentDescription = "Boton desplegable",
            modifier = Modifier.requiredWidth(50.dp)
        )

    }

}

@Composable
private fun Description(affirmation: Affirmation, modifier: Modifier = Modifier) {
    val context = LocalContext.current
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier
            .fillMaxSize()
            .padding(8.dp)
    ) {
        Column(
            modifier = Modifier.padding(
                start = 16.dp,
                top = 8.dp,
                bottom = 16.dp,
                end = 16.dp
            )
        ) {
            Text(
                text = "Descripcion: ",
                fontSize = 13.sp,
            )
            Text(
                text = stringResource(affirmation.stringResourceText),
                fontSize = 15.sp,
                modifier = Modifier
                    .padding(8.dp),
                overflow = TextOverflow.Ellipsis,
                maxLines = 2
            )
        }
    }
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier
            .fillMaxSize()
            .padding(4.dp)
    ) {
        Spacer(Modifier.padding(10.dp))
        TextButton(onClick = {
            val intent = Intent(context, DetailActivity::class.java)
            intent.putExtra("id", affirmation.id)
            context.startActivity(intent)
        }) {
            Text("See more")
        }
    }
}

